package internal

import (
	"context"
	"net/http"

	"golang.org/x/sync/errgroup"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/prometheus/config"
	"github.com/prometheus/prometheus/scrape"
)

type ScrapeTarget struct {
	Client       *http.Client
	Target       *scrape.Target
	ScrapeConfig *config.ScrapeConfig
}

func Targets(ctx context.Context, logger log.Logger, cctgCh <-chan *ConfigClientsTargetGroups) (<-chan []*ScrapeTarget, func() error) {
	out := make(chan []*ScrapeTarget)
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		defer close(out)
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case cctg, active := <-cctgCh:
				if !active {
					return nil
				}
				var all []*ScrapeTarget
				for jobName, tgs := range cctg.TargetGroups {
					client := cctg.Clients[jobName]
					if client == nil {
						continue
					}
					var sc *config.ScrapeConfig
					for _, cur := range cctg.Config.ScrapeConfigs {
						if cur.JobName != jobName {
							continue
						}
						sc = cur
						break
					}
					if sc == nil {
						continue
					}
					for _, tg := range tgs {
						targets, err := scrape.TargetsFromGroup(tg, sc)
						if err != nil {
							return err
						}
						for _, t := range targets {
							all = append(all, &ScrapeTarget{
								Client:       client,
								Target:       t,
								ScrapeConfig: sc,
							})
						}
					}
				}
				select {
				case <-ctx.Done():
					return ctx.Err()
				case out <- all:
				}
			}
		}
	})
	return out, eg.Wait
}
