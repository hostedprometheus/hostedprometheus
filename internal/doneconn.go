package internal

import (
	"context"
	"net"
	"time"
)

var _ net.Conn = &dconn{}

// dconn implements a net.Conn but adds the Done() <-chan struct{} method
// which acts like a context.Context.Done() for signalling when the connection
// is closed.
type dconn struct {
	cancel context.CancelFunc
	conn   net.Conn
	ctx    context.Context
}

func newDonn(conn net.Conn) *dconn {
	ctx, cancel := context.WithCancel(context.Background())
	return &dconn{
		cancel: cancel,
		conn:   conn,
		ctx:    ctx,
	}
}

func (dc *dconn) Done() <-chan struct{} {
	return dc.ctx.Done()
}

func (dc *dconn) Read(b []byte) (n int, err error)  { return dc.conn.Read(b) }
func (dc *dconn) Write(b []byte) (n int, err error) { return dc.conn.Write(b) }
func (dc *dconn) Close() error {
	defer dc.cancel()
	return dc.conn.Close()
}
func (dc *dconn) LocalAddr() net.Addr                { return dc.conn.LocalAddr() }
func (dc *dconn) RemoteAddr() net.Addr               { return dc.conn.RemoteAddr() }
func (dc *dconn) SetDeadline(t time.Time) error      { return dc.conn.SetDeadline(t) }
func (dc *dconn) SetReadDeadline(t time.Time) error  { return dc.conn.SetReadDeadline(t) }
func (dc *dconn) SetWriteDeadline(t time.Time) error { return dc.conn.SetWriteDeadline(t) }
