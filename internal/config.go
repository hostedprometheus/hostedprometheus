package internal

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	config_util "github.com/prometheus/common/config"
	"github.com/prometheus/prometheus/config"
	"golang.org/x/sync/errgroup"
)

// ConfigAndClients represents a change in config value and pre-allocated http clients
// per-job as configured by the config.
type ConfigAndClients struct {
	Config  *config.Config
	Clients map[string]*http.Client
}

func LoadFile(filename string) (*ConfigAndClients, error) {
	cac := &ConfigAndClients{
		Clients: map[string]*http.Client{},
	}
	cfg, err := config.LoadFile(filename)
	if err != nil {
		return nil, err
	}
	cac.Config = cfg
	for _, sc := range cfg.ScrapeConfigs {
		client, err := config_util.NewClientFromConfig(sc.HTTPClientConfig, sc.JobName)
		if err != nil {
			return nil, err
		}
		cac.Clients[sc.JobName] = client
	}
	return cac, nil
}

// Config reads the provided filename to parse a prometheus config struct. If the file
// is invalid, then the returned channel is closed and the wait function returns an error
// explaining the failure.
// The caller may signal reparsing of the config using the tigger channel. If a parse
// failure occurs after the first successful config parse, the channel will not terminate.
// Rather, a log message explaining why the config couldn't be reloaded is printed and
// the loop continues waiting for another trigger to reparse the file.
func Config(ctx context.Context, logger log.Logger, filename string, trigger <-chan struct{}) (<-chan *ConfigAndClients, func() error) {
	out := make(chan *ConfigAndClients)
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		defer close(out)
		logger = log.With(logger, "filename", filename)
		level.Info(logger).Log("msg", "loading config")
		cac, err := LoadFile(filename)
		if err != nil {
			return err
		}
		select {
		case <-ctx.Done():
			return ctx.Err()
		case out <- cac:
		}
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-trigger:
				level.Info(logger).Log("msg", "loading config")
				cac, err := LoadFile(filename)
				if err != nil {
					return err
				}
				if err != nil {
					level.Error(logger).Log("msg", "unable to reload config; continuing operation with last known good config")
					continue
				}
				select {
				case <-ctx.Done():
					return ctx.Err()
				case out <- cac:
				}
			}
		}
	})
	return out, eg.Wait
}
