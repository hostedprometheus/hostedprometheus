package internal

import (
	"context"
	"net"

	"golang.org/x/sync/errgroup"
)

// Redial turns a dialer function into a channel of net.Conn where the produced net.Conn
// is watched for when it is closed and a new net.Conn is produced in its place.
func Redial(ctx context.Context, dialer func(context.Context) (net.Conn, error)) (<-chan net.Conn, func() error) {
	eg, ctx := errgroup.WithContext(ctx)
	out := make(chan net.Conn)
	eg.Go(func() error {
		defer close(out)
		for {
			conn, err := dialer(ctx)
			if err != nil {
				return err
			}
			dconn := newDonn(conn)
			// send conn
			select {
			case <-ctx.Done():
				return ctx.Err()
			case out <- dconn:
			}
			// await conn close
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-dconn.Done():
			}
		}
	})
	return out, eg.Wait
}
