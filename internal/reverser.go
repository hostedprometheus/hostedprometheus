package internal

import (
	"context"
	"net"

	"github.com/pkg/errors"
)

var _ net.Listener = &Reverser{}

// Reverser consumes a channel of pre-established net.Conn connections and
// exposes them via the net.Listener interface.
type Reverser struct {
	cancel context.CancelFunc
	connCh <-chan net.Conn
	ctx    context.Context
}

func NewReverser(connCh <-chan net.Conn) *Reverser {
	ctx, cancel := context.WithCancel(context.Background())
	r := &Reverser{
		cancel: cancel,
		connCh: connCh,
		ctx:    ctx,
	}
	return r
}

func (r *Reverser) Accept() (net.Conn, error) {
	select {
	case <-r.ctx.Done():
		return nil, r.ctx.Err()
	case conn, active := <-r.connCh:
		if !active {
			return nil, errors.New("reverser conn channel has shut down")
		}
		return conn, nil
	}
}

func (r *Reverser) Close() error {
	r.cancel()
	return nil
}

func (r *Reverser) Addr() net.Addr {
	return _addr
}

type addr struct{}

var _addr = &addr{}

func (a *addr) Network() string { return "rgrpc" }
func (a *addr) String() string  { return "" }
