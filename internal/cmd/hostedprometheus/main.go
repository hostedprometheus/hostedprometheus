package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"
	"github.com/prometheus/common/promlog"
	promlogflag "github.com/prometheus/common/promlog/flag"
	"gitlab.com/hostedprometheus/hostedprometheus/internal"
	"golang.org/x/sync/errgroup"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	k8s_runtime "k8s.io/apimachinery/pkg/util/runtime"
)

func main() {
	var logger log.Logger
	logger = log.NewNopLogger()
	ctx, cancel := context.WithCancel(context.Background())
	term := make(chan os.Signal, 1)
	signal.Notify(term, os.Interrupt)
	go func() {
		<-term
		level.Debug(logger).Log("msg", "shuting down...")
		cancel()
		<-term
		os.Exit(1)
	}()
	sighup := make(chan os.Signal, 1)
	signal.Notify(sighup, syscall.SIGHUP)
	cfgTrigger := make(chan struct{})
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return nil
			case <-sighup:
				select {
				case <-ctx.Done():
					return nil
				case cfgTrigger <- struct{}{}:
				}
			}
		}
	})
	a := kingpin.New(filepath.Base(os.Args[0]), "The HostedPrometheus remote scraping bridge")
	a.Version("hostedprometheus-v1.0.0-alpha1")
	a.HelpFlag.Short('h')
	var apiEndpoint string
	var apiKey string
	var configFile string
	var webListenAddress string
	var logLevel promlog.AllowedLevel
	a.Flag("config.file", "Prometheus configuration file path.").Default("prometheus.yml").StringVar(&configFile)
	a.Flag("hostedprometheus.key", "HostedPrometheus API key").StringVar(&apiKey)
	a.Flag("hostedprometheus.endpoint", "HostedPrometheus API endpoint").Default("https://bridge.hostedprometheus.com/").StringVar(&apiEndpoint)
	a.Flag("web.listen-address", "Address to listen on for UI, API, and telemetry.").Default("0.0.0.0:9090").StringVar(&webListenAddress)
	promlogflag.AddFlags(a, &logLevel)
	_, err := a.Parse(os.Args[1:])
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "Error parsing commandline arguments"))
		a.Usage(os.Args[1:])
		os.Exit(2)
	}
	logger = promlog.New(logLevel)
	k8s_runtime.ErrorHandlers = []func(error){
		func(err error) {
			level.Error(log.With(logger, "component", "k8s_client_runtime")).Log("err", err)
		},
	}
	level.Info(logger).Log("msg", "Starting HostedPrometheus", "version", "v1.0.0-alpha1")
	dialer, err := internal.MakeDialer(logger, apiEndpoint)
	if err != nil {
		panic(err)
	}
	s := &internal.Server{
		Key: apiKey,
	}
	eg.Go(func() error {
		tgts, waitTgts := internal.FilenameToTargets(ctx, logger, configFile, cfgTrigger)
		connCh, waitConn := internal.Redial(ctx, dialer)
		eg.Go(waitTgts)
		eg.Go(waitConn)
		lis := internal.NewReverser(connCh)
		err = s.Run(ctx, logger, lis, tgts)
		if err != nil {
			return err
		}
		return nil
	})
	eg.Go(func() error {
		lis, err := net.Listen("tcp", webListenAddress)
		if err != nil {
			return err
		}
		mux := http.NewServeMux()
		mux.HandleFunc("/scrape", s.ScrapeHTTP)
		mux.HandleFunc("/targets", s.TargetsHTTP)
		svr := http.Server{
			Handler: mux,
		}
		go func() {
			<-ctx.Done()
			svr.Close()
		}()
		return svr.Serve(lis)
	})
	err = eg.Wait()
	if err != nil {
		panic(err)
	}
}
