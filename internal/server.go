package internal

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/http"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/prometheus/pkg/labels"
	"github.com/prometheus/prometheus/pkg/textparse"
	targeter "gitlab.com/hostedprometheus/hostedprometheus/internal/protogen/hostedprometheus/targeter/v1"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
)

var _ targeter.TargeterServer = &Server{}

type Server struct {
	// API key to make available to info requests
	Key string

	targets        map[string]*ScrapeTarget
	targetsVersion int64
	targetsCond    *sync.Cond
}

func (s *Server) TargetsHTTP(w http.ResponseWriter, r *http.Request) {
	var labels []string
	s.targetsCond.L.Lock()
	for k := range s.targets {
		labels = append(labels, k)
	}
	s.targetsCond.L.Unlock()
	for _, k := range labels {
		fmt.Fprintf(w, "%s\n", k)
	}
	return
}

func (s *Server) ScrapeHTTP(w http.ResponseWriter, r *http.Request) {
	target := r.URL.Query().Get("target")
	var st *ScrapeTarget
	s.targetsCond.L.Lock()
	st = s.targets[target]
	s.targetsCond.L.Unlock()
	if st == nil {
		http.Error(w, "not found", http.StatusNotFound)
		return
	}
	Scrape(r.Context(), st, w)
	return
}

func (s *Server) Run(ctx context.Context, logger log.Logger, l net.Listener, targets <-chan []*ScrapeTarget) error {
	eg, ctx := errgroup.WithContext(ctx)
	s.targets = map[string]*ScrapeTarget{}
	s.targetsCond = sync.NewCond(&sync.Mutex{})
	svr := grpc.NewServer()
	targeter.RegisterTargeterServer(svr, s)
	eg.Go(func() error {
		return svr.Serve(l)
	})
	go func() {
		<-ctx.Done()
		svr.Stop()
	}()
	eg.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case ts, active := <-targets:
				if !active {
					return nil
				}
				next := map[string]*ScrapeTarget{}
				s.targetsCond.L.Lock()
				for _, t := range ts {
					b, err := t.Target.Labels().MarshalJSON()
					if err != nil {
						s.targetsCond.L.Unlock()
						return err
					}
					next[string(b)] = t
				}
				s.targetsVersion++
				s.targets = next
				s.targetsCond.L.Unlock()
				s.targetsCond.Broadcast()
			}
		}
	})
	return eg.Wait()
}

func (s *Server) Targets(ctx context.Context, req *targeter.TargetsRequest) (*targeter.TargetsResponse, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	var done bool
	go func() {
		<-ctx.Done()
		done = true
		s.targetsCond.Broadcast()
	}()
	s.targetsCond.L.Lock()
	for {
		if done {
			s.targetsCond.L.Unlock()
			return nil, ctx.Err()
		}
		if s.targetsVersion != req.Version {
			break
		}
		s.targetsCond.Wait()
	}
	nextVersion := s.targetsVersion
	defer s.targetsCond.L.Unlock()
	resp := &targeter.TargetsResponse{
		Version: nextVersion,
	}
	for _, t := range s.targets {
		resp.Targets = append(resp.Targets, &targeter.Target{
			Labels: t.Target.Labels().Map(),
		})
	}
	return resp, nil
}

type Sample struct {
	Labels    map[string]string
	Timestamp int64
	Value     float64
}

func (s *Server) Scrape(ctx context.Context, req *targeter.ScrapeRequest) (*targeter.ScrapeResponse, error) {
	b, err := json.Marshal(req.Target.Labels)
	if err != nil {
		return nil, err
	}
	target := string(b)
	var st *ScrapeTarget
	s.targetsCond.L.Lock()
	st = s.targets[target]
	s.targetsCond.L.Unlock()
	if st == nil {
		return nil, errors.New("not found")
	}
	buf := bytes.NewBuffer([]byte{})
	err = Scrape(ctx, st, buf)
	if err != nil {
		return nil, err
	}
	resp := &targeter.ScrapeResponse{}
	p := textparse.New(buf.Bytes())
	for p.Next() {
		var t int64
		_, ts, v := p.At()
		if ts != nil {
			t = *ts
		}
		var lset labels.Labels
		p.Metric(&lset)
		// TODO do metric relabling from config and also caller-provided relabling
		resp.Samples = append(resp.Samples, &targeter.Sample{
			Labels:    lset.Map(),
			Timestamp: t,
			Value:     v,
		})
	}
	err = p.Err()
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *Server) Info(ctx context.Context, req *targeter.InfoRequest) (*targeter.InfoResponse, error) {
	return &targeter.InfoResponse{
		Key: s.Key,
	}, nil
}
