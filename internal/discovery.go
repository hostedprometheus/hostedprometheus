package internal

import (
	"context"
	"net/http"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/prometheus/config"
	"github.com/prometheus/prometheus/discovery"
	sd_config "github.com/prometheus/prometheus/discovery/config"
	"github.com/prometheus/prometheus/discovery/targetgroup"
	"golang.org/x/sync/errgroup"
)

type ConfigClientsTargetGroups struct {
	Config       *config.Config
	Clients      map[string]*http.Client
	TargetGroups map[string][]*targetgroup.Group
}

func Discovery(ctx context.Context, logger log.Logger, cfgCh <-chan *ConfigAndClients) (<-chan *ConfigClientsTargetGroups, func() error) {
	out := make(chan *ConfigClientsTargetGroups)
	cctg := &ConfigClientsTargetGroups{}
	var l sync.Mutex
	eg, ctx := errgroup.WithContext(ctx)
	dm := discovery.NewManager(ctx, logger)
	eg.Go(func() error {
		return dm.Run()
	})
	eg.Go(func() error {
		defer close(out)
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case cac, active := <-cfgCh:
				if !active {
					return nil
				}
				l.Lock()
				cctg.Config = cac.Config
				cctg.Clients = cac.Clients
				l.Unlock()
				c := make(map[string]sd_config.ServiceDiscoveryConfig)
				for _, v := range cac.Config.ScrapeConfigs {
					c[v.JobName] = v.ServiceDiscoveryConfig
				}
				err := dm.ApplyConfig(c)
				if err != nil {
					return err
				}
			}
		}
	})
	eg.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case d, active := <-dm.SyncCh():
				if !active {
					return nil
				}
				l.Lock()
				cctg.TargetGroups = d
				l.Unlock()
				select {
				case <-ctx.Done():
					return ctx.Err()
				case out <- cctg:
				}
			}
		}
	})
	return out, eg.Wait
}
