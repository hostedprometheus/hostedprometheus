package internal

import (
	"context"

	"github.com/go-kit/kit/log"
	"golang.org/x/sync/errgroup"
)

func FilenameToTargets(ctx context.Context, logger log.Logger, filename string, reload <-chan struct{}) (<-chan []*ScrapeTarget, func() error) {
	eg, ctx := errgroup.WithContext(ctx)
	cfg, wait := Config(ctx, logger, filename, reload)
	eg.Go(wait)
	disc, wait := Discovery(ctx, logger, cfg)
	eg.Go(wait)
	tgts, wait := Targets(ctx, logger, disc)
	eg.Go(wait)
	return tgts, eg.Wait
}
