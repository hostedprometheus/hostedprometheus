package internal

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"
)

func MakeDialer(logger log.Logger, rawuri string) (func(context.Context) (net.Conn, error), error) {
	u, err := url.Parse(rawuri)
	if err != nil {
		return nil, errors.Wrap(err, "url parse")
	}
	if u.Scheme != "https" {
		return nil, fmt.Errorf("target uri must be https targeturi=%s", rawuri)
	}
	host := u.Host
	addr := u.Host
	if !strings.Contains(host, ":") {
		addr = addr + ":443"
	} else {
		parts := strings.SplitN(host, ":", 2)
		host = parts[0]
	}
	dialer := func(ctx context.Context) (net.Conn, error) {
		var conn net.Conn
		d := net.Dialer{}
		operation := func() error {
			level.Info(logger).Log("msg", "dialing", "addr", addr)
			_conn, err := d.DialContext(ctx, "tcp", addr)
			if err != nil {
				return err
			}
			tconn := tls.Client(_conn, &tls.Config{
				ServerName: host,
			})
			// TODO allow timeout
			err = tconn.Handshake()
			if err != nil {
				_conn.Close()
				return errors.Wrap(err, "tls handshake")
			}
			conn = tconn
			return nil
		}
		// TODO replace with better backoff logic
		t := time.NewTicker(time.Minute)
		defer t.Stop()
		for {
			err := operation()
			if err == nil {
				return conn, nil
			}
			level.Warn(logger).Log("msg", "unable to dial but will retry later")
			select {
			case <-ctx.Done():
				return nil, ctx.Err()
			case <-t.C:
			}
		}
	}
	return dialer, nil
}
