package internal

import (
	"bufio"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"net/http"
	"time"
)

const acceptHeader = `text/plain;version=0.0.4;q=1,*/*;q=0.1`

var userAgentHeader = fmt.Sprintf("HostedPrometheus/%s", "v1.0.0-alpha1")

func Scrape(ctx context.Context, st *ScrapeTarget, w io.Writer) error {
	req, err := http.NewRequest("GET", st.Target.URL().String(), nil)
	if err != nil {
		return err
	}
	req.Header.Add("Accept", acceptHeader)
	req.Header.Add("Accept-Encoding", "gzip")
	req.Header.Set("User-Agent", userAgentHeader)
	req.Header.Set("X-Prometheus-Scrape-Timeout-Seconds", fmt.Sprintf("%f", time.Duration(st.ScrapeConfig.ScrapeTimeout).Seconds()))
	req = req.WithContext(ctx)
	resp, err := st.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("server returned HTTP status %s", resp.Status)
	}
	if resp.Header.Get("Content-Encoding") != "gzip" {
		_, err = io.Copy(w, resp.Body)
		return err
	}
	buf := bufio.NewReader(resp.Body)
	gz, err := gzip.NewReader(buf)
	if err != nil {
		return err
	}
	defer gz.Close()
	_, err = io.Copy(w, gz)
	return err
}
